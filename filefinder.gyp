{
  'variables': {
    'filefinder_path': 'filefinder',
    'alchemy_path': 'alchemy',
    'cef_path': 'cef',
  },
  'target_defaults': {
    'conditions': [
      ['OS=="linux"', {'defines': ['__LINUX__']}],
      ['OS=="mac"', {
        'defines': ['__MAC__'],
        'xcode_settings': {
          'CLANG_CXX_LANGUAGE_STANDARD': 'c++0x',
          'CLANG_CXX_LIBRARY': 'libc++'
        }
      }],
      ['OS=="win"', {
        'defines': [
          '__WIN__',
          'UNICODE',
          'NOMINMAX',
        ]
      }],
    ],
    'cflags': [
      '-Wall',
      '-std=c++0x'
    ],
    'msbuild_toolset': 'v120_CTP_Nov2012',
    'configurations': {
      'Debug': {
        'defines': [
          'DEBUG',
        ],
        'msvs_settings': {
          'VCCLCompilerTool': {
            'Optimization': '0',
          },
          'VCLinkerTool': {
            'GenerateDebugInformation': 'true',
          },
        },
        'xcode_settings': {
          'GCC_GENERATE_DEBUGGING_SYMBOLS ': 'YES',
          'GCC_DEBUGGING_SYMBOLS': 'full',
          'DEBUG_INFORMATION_FORMAT': 'dwarf-with-dsym',
          'GCC_OPTIMIZATION_LEVEL': '0',
        },
      },
      'Release': {
        'defines': [
          'RELEASE',
        ],
      },
    },
  },
  'targets': [
    {
      # produces an executable on linux, an app bundle on mac
      'target_name': 'filefinder',
      'type': 'executable',
      'mac_bundle': 1,
      'include_dirs': [
        '<(alchemy_path)',
      ],
      'sources': [
        '<(filefinder_path)/src/main.cpp',
      ],
      'conditions': [
        ['OS=="linux"', {
          'include_dirs': [
            '/home/matt/Documents/boost/boost_1_53_0'
          ],
          'cflags': [
            '<!@(pkg-config --cflags gtk+-2.0 gthread-2.0)',
          ],
          'link_settings': {
            'ldflags': [
              '<!@(pkg-config --libs-only-L --libs-only-other gtk+-2.0 gthread-2.0)',
              '-L/home/matt/Documents/boost/boost_1_53_0/stage/lib'
            ],
            'libraries': [
              '<!@(pkg-config --libs-only-l gtk+-2.0 gthread-2.0)',
              '<(alchemy_path)/alchemy.so',
              '<(cef_path)/libcef.so',
              '<(cef_path)/libcef_dll_wrapper.a',
              '-lboost_filesystem',
              '-lboost_system'
            ],
          },
          'copies': [
            {
              'destination': '<(PRODUCT_DIR)/locales',
              'files': ['<(cef_path)/locales/en-US.pak']
            },
            {
              'destination': '<(PRODUCT_DIR)',
              'files': ['<(cef_path)/chrome.pak']
            },
            {
              'destination': '<(PRODUCT_DIR)',
              'files': ['<(cef_path)/libcef.so']
            },
            {
              'destination': '<(PRODUCT_DIR)',
              'files': ['<(filefinder_path)/ui']
            }
          ],
        }],
        ['OS=="mac"', {
          'libraries': [
            '$(SDKROOT)/System/Library/Frameworks/AppKit.framework',
            '<(alchemy_path)/alchemy.dylib',
            '<(cef_path)/libcef.dylib',
            'libboost_filesystem.a',
            'libboost_system.a',
          ],
          'copies': [
            {
              'destination': '<(PRODUCT_DIR)/filefinder.app/Contents/MacOS',
              'files': ['<(cef_path)/libcef.dylib']
            },
            {
              'destination': '<(PRODUCT_DIR)/filefinder.app/Contents',
              'files': [
                '<(cef_path)/Resources',
                '<(filefinder_path)/ui',
              ]
            }
          ],
        }],
        ['OS=="win"', {
          'defines': [
            'UNICODE',
          ],
          'libraries': [
            '<(alchemy_path)/alchemy.lib',
            '<(cef_path)/libcef_dll_wrapper.lib',
            '-llibboost_filesystem-vc110-mt-1_53.lib',
            '-llibboost_system-vc110-mt-1_53.lib',
          ],
          'copies': [
            {
              'destination': '<(PRODUCT_DIR)/',
              'files': [
                '<(cef_path)/runtime/d3dcompiler_43.dll',
                '<(cef_path)/runtime/d3dx9_43.dll',
                '<(cef_path)/runtime/devtools_resources.pak',
                '<(cef_path)/runtime/icudt.dll',
                '<(cef_path)/runtime/libcef.dll',
                '<(cef_path)/runtime/libEGL.dll',
                '<(cef_path)/runtime/libGLESv2.dll',
                '<(cef_path)/libcef.lib',
              ]
            },
            {
              'destination': '<(PRODUCT_DIR)/locales',
              'files': ['<(cef_path)/runtime/locales/en-US.pak']
            },
            {
              'destination': '<(PRODUCT_DIR)/ui',
              'files': [
                '<(filefinder_path)/ui/index.html'
              ]
            },
          ],
        }],
      ],
    },
  ],
}
