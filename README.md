# File Finder

This is the "File Finder" example application we used while developing [Alchemy](http://libalchemy.org).

It demonstrates a real-world usage of Alchemy to create a file searching tool.

# Compilation

First, you need to obtain Alchemy and CEF binaries and headers. Prebuilt packages are available at [the Alchemy website](http://libalchemy.org/downloads.html).

Put the CEF files in the `CEF` directory, and the Alchemy files in the `Alchemy` directory.

Next, you'll need the GYP tool if you don't already have it. Instructions are available at the [Alchemy downloads page](http://libalchemy.org/reference/gyp.html). Navigate to this directory in your command line, and run the GYP executable to generate a platform-specific build project (XCode on Mac, Visual Studio on Windows, and Make on Linux).

Finally, build the project with your build tool / IDE.