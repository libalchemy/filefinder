#include "alchemy/alchemy.h"
#include <boost/filesystem.hpp>

#include <functional>
#include <iostream>

using namespace std;
using namespace std::placeholders;

struct file {
  string fullpath;
  string filepath;
  int size;
};

/* Method Declarations */
void updateDirectory(string directory);
void backUpdateDirectory();
void addFile(file f);
void emptyFrontEnd();
void fileSearch();

/* Globals */
unique_ptr<Alchemy::Application> app;
bool dirCancel = false;
vector<file> masterList;
string dir = "";
string oldDir = " ";
bool first = true;
mutex dirCancelMutex;

/* Methods */

//Called from front end. Sets directory and a directory cancel flag.
void updateDirectory(string directory) {
  if(!first){
    dirCancelMutex.lock();
    dirCancel = true;
    dirCancelMutex.unlock();
    oldDir = dir;
    dir = directory;
  } else {
    first = false;
  }
}

//Does the actual work of clearing master list, and checking if it is actually a directory.
void backUpdateDirectory(){
  boost::filesystem::path p(dir);
  if(boost::filesystem::is_directory(p)){
    emptyFrontEnd();
    masterList.clear();
    dirCancelMutex.lock();
    dirCancel = false;
    dirCancelMutex.unlock();
  } else {
    dir = oldDir;
    emptyFrontEnd();
    masterList.clear();
    app->Page("index.html")->Call("showError", "Not a directory, displaying last known good directory.");
  }
}

//Adds a file to master list and front end.
void addFile(file f) {
  masterList.push_back(f);
  app->Page("index.html")->Call("addFile", f.fullpath, f.size);
}

//Removes files from front end.
void emptyFrontEnd(){
  for (int i = 0; i < masterList.size(); ++i){
    //Remove files from front end.
  }
}

//The file searching algorithm.
void fileSearch(){
  while (true) {
    stack<boost::filesystem::path> stack;
    boost::filesystem::path p(dir);
    stack.push(p);
    while (stack.size() > 0) {
      boost::filesystem::path p = stack.top();
      stack.pop();
      boost::filesystem::directory_iterator end_itr;
      for(boost::filesystem::directory_iterator itr(p); itr != end_itr; ++itr) {
        if (boost::filesystem::is_directory(itr->path())) {
          stack.push(itr->path());
        } else {
            try{
              file f;
              f.size = boost::filesystem::file_size(itr->path());
              f.filepath = boost::filesystem::basename(itr->path()) + boost::filesystem::extension(itr->path());
              f.fullpath = itr->path().string();
              addFile(f);
            } catch (const boost::filesystem::filesystem_error& ex){}
        }
      }
    }
    while(true){
      dirCancelMutex.lock();
      if(dirCancel != false){
        dirCancelMutex.unlock();
        backUpdateDirectory();
        break;
      }
      dirCancelMutex.unlock();
    }
  }
}

int main(){
  Alchemy::Settings settings;
  settings.Directory("ui");
  settings.DefaultWindowSize(Alchemy::Size(1100, 800));

  app = Alchemy::Application::Create(settings);

  auto index = app->Page("index.html");
  index->Bind("updateDirectory", updateDirectory);
  index->Bind("getExecutableFolder", Alchemy::GetExecutableFolder);
  dir = Alchemy::GetExecutableFolder();
  
  auto dirthread = thread([&](){
    while(!index->Ready()){
      this_thread::sleep_for(chrono::milliseconds(100));
    }
    fileSearch();
  });

  app->Show(index);
  dirthread.join();
  return 0;
}