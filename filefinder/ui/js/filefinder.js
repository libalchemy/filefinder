function front_to_back(name) {
  if (!window.app) window.app = {};
  if (!window.app[name]) window.app[name] = function () {
    console.log('call back-end',name,arguments);
  };
}
function back_to_front(name, f) {
  if (!window.app) window.app = {};
  if (!window.app[name]) window.app[name] = f.bind(ffVM);
}

// stolen from underscore.js
function throttle(func, wait) {
  var context, args, timeout, result;
  var previous = 0;
  var later = function() {
    previous = new Date();
    timeout = null;
    result = func.apply(context, args);
  };
  return function() {
    var now = new Date();
    var remaining = wait - (now - previous);
    context = this;
    args = arguments;
    if (remaining <= 0) {
      clearTimeout(timeout);
      timeout = null;
      previous = now;
      result = func.apply(context, args);
    } else if (!timeout) {
      timeout = setTimeout(later, remaining);
    }
    return result;
  };
}

function FileFinderViewModel() {
  this.results = ko.observableArray();
  this.directory = ko.observable('');
  this.query = ko.observable('');
  this.selectedResult = ko.observable(null);
  this.filteredResults = ko.computed(function(){
    var q = this.query().trim();
    if (q.length === 0) return this.results();

    return ko.utils.arrayFilter(this.results(), function(resultVM) {
      return resultVM.matches();
    });
  }, this);
}

FileFinderViewModel.prototype = {
  openFile: function(result) {
    this.selectedResult(result);
    $('#fileview').hide().fadeIn();
  },

  closeFile: function(result) {
    $('#fileview').fadeOut(function(){
      this.selectedResult(null);
    });
  },

  showError: function(message) {
    TINY.box.show({
      html: '<strong class="text-error">'+message+'</strong>',
      close: false,
      boxid: 'errormessage'
    });
  },

  addResult: function(filepath, size) {
    this.results.push(new ResultViewModel(filepath, size));
  },

  animateAdd: function(domNode, i, result) {
    if (domNode.nodeType == 1)
      $(domNode).hide().slideDown();
  },
  animateRemove: function(domNode, i, result) {
    if (domNode.nodeType == 1)
      $(domNode).slideUp(function(){ $(domNode).remove(); });
  }
};

var ffVM = new FileFinderViewModel();

function ResultViewModel(filepath, size) {
  var lastdot = filepath.lastIndexOf('.');

  var ext = filepath.slice(lastdot+1);
  this.isImage = false;
  if(ext === "jpg" || ext === "png" || ext === "gif"){
    this.iconClass = "result-icon-thumbnail";
    this.isImage = true;
  } else if (lastdot === -1) {
    this.iconClass = '';
  } else {
    this.iconClass = 'result-icon-'+ext;
  }

  this.size = size;
  this.filepath = filepath;

  var pieces = filepath
    .replace('\\','/')
    .split('/');

  var filename = pieces.splice(-1)[0];
  this.directory = pieces
    .slice(-5)
    .map(function(piece){
      if (piece.length > 10) {
        return piece.slice(0,7)+'...';
      }
      return piece;
    })
    .join('/');
  if (this.directory.length) this.directory += '/';

  this.filenameHtml = ko.computed(function(){
    var query = ffVM.query();
    var j = 0, i = 0;
    var filenameout = '';
    for (; i < filename.length; i++) {
      if (j < query.length && filename[i] === query[j]) {
        j++;
        filenameout += '<span class="highlight">' + filename[i] + '</span>';
      } else {
        filenameout += filename[i];
      }
    }
    return filenameout;
  });

  this.matches = ko.computed(function(){
    var query = ffVM.query().trim();
    var j = 0, i = 0;

    for (; i < filename.length; i++) {
      if (j < query.length && filename[i] == query[j]) j++;
      else if (j == query.length) return true;
    }
    return j == query.length;
  });
}

front_to_back('updateDirectory');
front_to_back('getExecutableFolder');
back_to_front('addFile', ffVM.addResult);
back_to_front('showError', ffVM.showError);

$(function(){
  ko.applyBindings(ffVM);
  $('._fancy input').fancyInput();
  ffVM.directory.subscribe(app.updateDirectory);
  var dir = app.getExecutableFolder();
  ffVM.directory(dir);
  fancyInput.fillText(dir, document.getElementById('folder-input'));
});